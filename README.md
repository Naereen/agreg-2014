# [Agrégation de mathématiques (2014)](http://perso.crans.org/besson/agreg-2014/)
Ce [dépôt git](https://bitbucket.org/lbesson/agreg-2014/) contient ma **liste de développements** (maths et info), et mes **références bibliographiques** pour [l'agrégation de mathématiques](http://agreg.org/), [option informatique](http://agreg-cachan.fr/info/).

Veuillez consulter [cette page](http://perso.crans.org/besson/agreg-2014/) svp.

----

## Licence d'utilisation ?
Ces fichiers sont tous publiquement distribués, [sous la licence MIT](http://lbesson.mit-license.org/).
